package net.thebugmc.error;

final class ResultException extends RuntimeException {
    public ResultException(Throwable exception) {
        super(exception);
    }
}
