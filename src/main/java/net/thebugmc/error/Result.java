package net.thebugmc.error;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.*;
import java.util.stream.Collector;
import java.util.stream.Stream;

import static java.util.Collections.reverse;
import static net.thebugmc.error.Functionals.*;
import static net.thebugmc.error.Unit.unit;

/**
 * Result immutable type: {@link Ok} or {@link Error}.
 *
 * @param <S> Success type.
 * @param <E> Error type.
 * @see Optional
 */
@SuppressWarnings("UnusedReturnValue")
public sealed interface Result<S, E> permits Result.Ok, Result.Error {
    //
    // Factory
    //

    record Ok<S, E>(S get) implements Result<S, E> {
        public Ok {
            if (get == null) throw new IllegalArgumentException("Supplied null to result");
        }

        public E getError() {
            throw new NoSuchElementException("Cannot get error of success");
        }

        public Optional<S> ok() {
            return some(get);
        }

        public Stream<S> okStream() {
            return Stream.of(get);
        }

        public Optional<E> error() {
            return none();
        }

        public Stream<E> errorStream() {
            return Stream.of();
        }

        public boolean isOk() {
            return true;
        }

        public boolean isError() {
            return false;
        }

        public String toString() {
            return get + "";
        }

        public int hashCode() {
            return get.hashCode();
        }

        public boolean equals(Object obj) {
            return obj instanceof Ok<?, ?> ok && get.equals(ok.get)
                   || obj instanceof Optional<?> opt && get.equals(opt.orElse(null));
        }

        //
        // Function
        //

        public Error<E, S> flip() {
            return Error(get);
        }

        public <S2, E2> Ok<S2, E2> map(Function<S, S2> sMapper, Function<E, E2> eMapper) {
            return mapOk(sMapper).mapError();
        }

        public <S2> Ok<S2, E> mapOk(Function<S, S2> mapper) {
            return Ok(mapper.apply(get));
        }

        public Ok<Unit, E> mapUnit() {
            return Ok();
        }

        public <E2> Ok<S, E2> mapError() {
            return Ok(get);
        }

        public <E2> Ok<S, E2> mapError(Function<E, E2> mapper) {
            return mapError();
        }

        public <S2> Result<S2, E> flatMap(Function<S, Result<S2, E>> mapper) {
            return mapper.apply(get);
        }

        public Ok<S, E> peek(Consumer<Result<S, E>> peeker) {
            peeker.accept(this);
            return this;
        }

        public Ok<S, E> peekOk(Consumer<S> peeker) {
            peeker.accept(get);
            return this;
        }

        public Ok<S, E> peekError(Consumer<E> peeker) {
            return this;
        }

        public Result<S, E> filter(Predicate<S> filter, Supplier<E> error) {
            // don't do anything if error or filter passed
            // error if ok but filter not passed
            return filter.test(get)
                ? this
                : Error(error.get());
        }

        public Ok<S, E> or(Result<S, E> other) {
            return this;
        }

        public Ok<S, E> or(Optional<S> other) {
            return this;
        }

        public Ok<S, E> flatOr(Supplier<Result<S, E>> other) {
            return this;
        }

        public <SOther, SCombined> Result<SCombined, E> and(
            Result<SOther, E> other,
            BiFunction<S, SOther, SCombined> combiner
        ) {
            return flatMap(s -> other.mapOk(applyF2(combiner, s)));
        }

        public Result<Unit, E> andUnit(Result<?, E> other) {
            return other.mapUnit();
        }

        public Result<Unit, E> flatAndUnit(Supplier<Result<?, E>> other) {
            return other.get().mapUnit();
        }
    }

    /**
     * Create a success {@link Result}.
     *
     * @param s   Success value.
     * @param <S> Success type.
     * @param <E> Error type.
     * @return Success result. Use {@link Optional} if you do not want error type.
     */
    static <S, E> Ok<S, E> Ok(S s) {
        return new Ok<>(s);
    }

    /**
     * Create a success {@link Result}.
     *
     * @param <E> Error type.
     * @return Success result. Use {@link Optional} if you do not want error type.
     */
    static <E> Ok<Unit, E> Ok() {
        return new Ok<>(unit());
    }

    record Error<S, E>(E getError) implements Result<S, E> {
        public Error {
            if (getError == null) throw new IllegalArgumentException("Supplied null to result");
        }

        public S get() {
            throw getError instanceof Throwable e
                ? new NoSuchElementException("Cannot unwrap error", e)
                : new NoSuchElementException("Cannot unwrap error: " + getError);
        }

        public Optional<S> ok() {
            return none();
        }

        public Stream<S> okStream() {
            return Stream.of();
        }

        public Optional<E> error() {
            return some(getError);
        }

        public Stream<E> errorStream() {
            return Stream.of(getError);
        }

        public boolean isOk() {
            return false;
        }

        public boolean isError() {
            return true;
        }

        public String toString() {
            return getError + "";
        }

        public int hashCode() {
            return getError.hashCode();
        }

        public boolean equals(Object obj) {
            return obj instanceof Error<?, ?> err && getError.equals(err.getError)
                   || obj instanceof Optional<?> opt && getError.equals(opt.orElse(null));
        }

        //
        // Function
        //

        public Ok<E, S> flip() {
            return Ok(getError);
        }

        public <S2, E2> Error<S2, E2> map(Function<S, S2> sMapper, Function<E, E2> eMapper) {
            return mapError(eMapper).mapOk();
        }

        public <S2> Error<S2, E> mapOk() {
            return Error(getError);
        }

        public <S2> Error<S2, E> mapOk(Function<S, S2> mapper) {
            return mapOk();
        }

        public Error<Unit, E> mapUnit() {
            return mapOk();
        }

        public <E2> Error<S, E2> mapError(Function<E, E2> mapper) {
            return Error(mapper.apply(getError));
        }

        public <S2> Error<S2, E> flatMap(Function<S, Result<S2, E>> mapper) {
            return mapOk();
        }

        public Error<S, E> peek(Consumer<Result<S, E>> peeker) {
            peeker.accept(this);
            return this;
        }

        public Error<S, E> peekOk(Consumer<S> peeker) {
            return this;
        }

        public Error<S, E> peekError(Consumer<E> peeker) {
            peeker.accept(getError);
            return this;
        }

        public Error<S, E> filter(Predicate<S> filter, Supplier<E> error) {
            return this;
        }

        public Result<S, E> or(Result<S, E> other) {
            return other.mapError(ignoreS(applyS(getError)));
        }

        public Result<S, E> or(Optional<S> other) {
            return from(other, applyS(error().get()));
        }

        public Result<S, E> flatOr(Supplier<Result<S, E>> other) {
            return other.get().mapError(ignoreS(applyS(error().get())));
        }

        public <SOther, SCombined> Error<SCombined, E> and(
            Result<SOther, E> other,
            BiFunction<S, SOther, SCombined> combiner
        ) {
            return mapOk();
        }

        public Error<Unit, E> andUnit(Result<?, E> other) {
            return mapOk();
        }

        public Error<Unit, E> flatAndUnit(Supplier<Result<?, E>> other) {
            return mapOk();
        }
    }

    /**
     * Create an error {@link Result}.
     *
     * @param e   Error value.
     * @param <S> Success type.
     * @param <E> Error type.
     * @return Error result.
     */
    static <S, E> Error<S, E> Error(E e) {
        return new Error<>(e);
    }

    /**
     * Convert a boolean into {@link Result}.
     *
     * @param test  Boolean to convert.
     * @param ok    Object supplied if true.
     * @param error Object supplied if false.
     * @param <S>   Success type.
     * @param <E>   Error type.
     * @return Ok if true, else error.
     */
    static <S, E> Result<S, E> check(boolean test, Supplier<S> ok, Supplier<E> error) {
        return test ? Ok(ok.get()) : Error(error.get());
    }

    /**
     * Convert a boolean into {@link Result}.
     *
     * @param test    Boolean to convert.
     * @param ifTrue  Result supplied if true.
     * @param ifFalse Result supplied if false.
     * @param <S>     Success type.
     * @param <E>     Error type.
     * @return Obvious.
     */
    static <S, E> Result<S, E> flatCheck(
        boolean test,
        Supplier<Result<S, E>> ifTrue,
        Supplier<Result<S, E>> ifFalse
    ) {
        return (test ? ifTrue : ifFalse).get();
    }

    /**
     * Convert an optional into {@link Result}.
     *
     * @param ok    Optional to check and turn into success result.
     * @param error Object supplied if optional is empty.
     * @param <S>   Success type.
     * @param <E>   Error type.
     * @return Ok with the optional's value if present, else error.
     */
    static <S, E> Result<S, E> from(Optional<S> ok, Supplier<E> error) {
        return ok
            .<Result<S, E>>map(Result::Ok)
            .orElseGet(applyFS(Result::Error, error));
    }

    /**
     * Convert an optional into {@link Result}.
     *
     * @param check Optional to check and turn into success result.
     * @param other Object supplied if optional is empty.
     * @param <S>   Success type.
     * @param <E>   Error type.
     * @return Ok with the optional's value if present, else other.
     */
    static <S, E> Result<S, E> flatFrom(Optional<S> check, Supplier<Result<S, E>> other) {
        return check
            .<Result<S, E>>map(Result::Ok)
            .orElseGet(other);
    }

    //
    // Boilerplate
    //

    /**
     * Get the success value.
     *
     * @return Success value.
     * @throws NoSuchElementException If result is unsuccessful.
     *                                Error may be attached as a cause if it is {@link Throwable}.
     */
    S get() throws NoSuchElementException;

    /**
     * Get the error value.
     *
     * @return Error value.
     * @throws NoSuchElementException If result is successful.
     */
    E getError() throws NoSuchElementException;

    /**
     * Convert this result from either variant to a more general result.
     *
     * @return This result.
     */
    default Result<S, E> result() {
        return this;
    }

    /**
     * Get success value.
     *
     * @return Success value if result is successful or empty.
     */
    Optional<S> ok();

    /**
     * Get success value.
     *
     * @return Success value if result is successful or empty.
     */
    Stream<S> okStream();

    /**
     * Get error value.
     *
     * @return Error value if result is unsuccessful or empty.
     */
    Optional<E> error();

    /**
     * Get error value.
     *
     * @return Error value if result is unsuccessful or empty.
     */
    Stream<E> errorStream();

    /**
     * Check if result is successful.
     *
     * @return True if success result.
     */
    boolean isOk();

    /**
     * Check if result is unsuccessful.
     *
     * @return True if error result.
     */
    boolean isError();

    //
    // Function
    //

    Result<E, S> flip();

    /**
     * Convert a result into another result.
     *
     * @param mapper Converter.
     * @param <SNew> New success type.
     * @param <ENew> New error type.
     * @return New result.
     */
    default <SNew, ENew> Result<SNew, ENew> map(Function<Result<S, E>, Result<SNew, ENew>> mapper) {
        return mapper.apply(this);
    }

    /**
     * Convert the success value.
     *
     * @param sMapper Success converter.
     * @param eMapper Error converter.
     * @param <SNew>  New success type.
     * @param <ENew>  New error type.
     * @return New result.
     */
    <SNew, ENew> Result<SNew, ENew> map(Function<S, SNew> sMapper, Function<E, ENew> eMapper);

    /**
     * Convert the success value.
     *
     * @param mapper Converter.
     * @param <SNew> New success type.
     * @return New result.
     */
    <SNew> Result<SNew, E> mapOk(Function<S, SNew> mapper);

    /**
     * Convert the success value to {@link Unit}.
     *
     * @return New result.
     */
    Result<Unit, E> mapUnit();

    /**
     * Convert the error value.
     *
     * @param mapper Converter.
     * @param <ENew> New error type.
     * @return New result.
     */
    <ENew> Result<S, ENew> mapError(Function<E, ENew> mapper);

    /**
     * Convert the success value to a new result.
     *
     * @param mapper Converter.
     * @param <SNew> New success type.
     * @return New result.
     */
    <SNew> Result<SNew, E> flatMap(Function<S, Result<SNew, E>> mapper);

    /**
     * Peek at the result without changing it.
     *
     * @param peeker Peeker function.
     */
    Result<S, E> peek(Consumer<Result<S, E>> peeker);

    /**
     * Peek at the success value without changing it, if it is present.
     *
     * @param peeker Peeker function.
     */
    Result<S, E> peekOk(Consumer<S> peeker);

    /**
     * Peek at the error value without changing it, if it is present.
     *
     * @param peeker Peeker function.
     */
    Result<S, E> peekError(Consumer<E> peeker);

    /**
     * Filter the {@link Ok} value, make an {@link Error} if not passed.
     *
     * @param filter Filter function.
     * @param error  Error supplier in case ok does not pass filter.
     */
    Result<S, E> filter(Predicate<S> filter, Supplier<E> error);

    /**
     * Try to receive a value from another result.
     *
     * @param other Other result to use if this result is unsuccessful.
     * @return Either this or other, but if other result fails, this error is used, ignoring
     * other's error.
     */
    Result<S, E> or(Result<S, E> other);

    /**
     * Try to receive a value from an optional.
     *
     * @param other Optional to use if this result is unsuccessful.
     * @return Either this or other, but if other result is empty, this error is used, ignoring
     * other's error.
     */
    Result<S, E> or(Optional<S> other);

    /**
     * Try to receive a value from another result.
     *
     * @param other Other result to use if this result is unsuccessful.
     * @return Either this or other, but if other result fails, this error is used, ignoring
     * other's error.
     */
    Result<S, E> flatOr(Supplier<Result<S, E>> other);

    /**
     * Ensure both results are successful.
     *
     * @param other       Other result to use if this result is successful.
     * @param combiner    Function that combines {@code this} result's success value with the
     *                    {@code other}'s success value.
     * @param <SOther>    The success type of the other value
     * @param <SCombined> The success type of the combined value
     * @return Combined results.
     */
    <SOther, SCombined> Result<SCombined, E> and(
        Result<SOther, E> other,
        BiFunction<S, SOther, SCombined> combiner
    );

    /**
     * Ensure both results are successful.
     *
     * @param other Other result to use if this result is successful.
     * @return Combined results, ignoring the value.
     * @see #toResultList() Collecting values instead of ignoring them
     */
    Result<Unit, E> andUnit(Result<?, E> other);

    /**
     * Ensure both results are successful.
     *
     * @param other Other result to use if this result is successful.
     * @return Combined results, ignoring the value.
     * @see #toResultList() Collecting values instead of ignoring them
     */
    Result<Unit, E> flatAndUnit(Supplier<Result<?, E>> other);

    //
    // Static Utils
    //

    /**
     * Flatten a {@link Result} of {@link Result}.
     *
     * @param result {@link Result} to flatten.
     * @param <S>    Success type.
     * @param <E>    Error type.
     * @return {@link Result} of {@link Result}.
     * @see #flatMap(Function)
     */
    static <S, E> Result<S, E> flatten(Result<Result<S, E>, E> result) {
        return result.flatMap(f());
    }

    /**
     * Get the success value or throw an exception.
     *
     * @param result {@link Result} to {@link #get()}.
     * @param <S>    Success type.
     * @param <E>    Error type.
     * @return Success value.
     * @throws E If result is unsuccessful.
     */
    static <S, E extends Throwable> S getOrThrow(Result<S, E> result) throws E {
        if (result.isError()) throw result.getError();
        return result.get();
    }

    /**
     * Catch an exception.
     *
     * @param ce  Exception type to catch.
     * @param s   {@link TryS} to catch.
     * @param <S> Success type.
     * @param <E> Error type.
     * @return {@link Ok} if successful, {@link Error} if caught an exception.
     * @see #tryGet(Class, TryF)
     */
    static <S, E extends Throwable> Result<S, E> tryGet(Class<E> ce, Supplier<S> s) {
        try {
            return Ok(s.get());
        } catch (ResultException e) {
            return errorCatchRethrow(ce, e);
        }
    }

    /**
     * Catch an exception.
     *
     * @param ce  Exception type to catch.
     * @param r   {@link TryR} to catch.
     * @param <E> Error type.
     * @return {@link Ok} if successful, {@link Error} if caught an exception.
     * @see #tryRun(Class, TryC)
     */
    static <E extends Throwable> Result<Unit, E> tryRun(Class<E> ce, Runnable r) {
        try {
            r.run();
            return Ok();
        } catch (ResultException e) {
            return errorCatchRethrow(ce, e);
        }
    }

    private static List<Throwable> tryCloseAll(List<AutoCloseable> resources) {
        reverse(resources); // reversing a stream does not make sense, so reverse a list
        return resources.stream()
            .map(i -> tryRun(
                Throwable.class,
                (TryR<Throwable>) i::close
            ))
            .flatMap(Result::errorStream)
            .toList(); // this lets us close all resources ignoring errors until collect
    }

    @SuppressWarnings("unchecked")
    private static <S, E extends Throwable> Error<S, E> errorCatchRethrow(
        Class<E> ce,
        ResultException e
    ) {
        var ex = e.getCause();
        if (ce.isInstance(ex))
            return Error((E) ex);
        throw e;
    }

    /**
     * Catch an exception.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import static net.thebugmc.error.Result.*;
     * import java.util.stream.Stream;
     *
     * var result1 = tryGet(Exception.class, r -> 42);
     * assertEquals(Ok(42), result1);
     *
     * var result2 = tryGet(Exception.class, r -> {
     *     throw new Exception();
     * });
     * assertTrue(result2.isError());
     *
     * var result3 = tryGet(Exception.class, r -> {
     *     return r.resource(Stream.of(1, 2, 3)).count();
     * });
     * assertEquals(Ok(3L), result3);
     *}
     *
     * @param ce  Exception type to catch.
     * @param f   TryWithResources block to catch.
     * @param <S> Success type.
     * @param <E> Error type.
     * @return {@link Ok} if successful, {@link Error} if caught an exception.
     */
    static <S, E extends Throwable> Result<S, E> tryGet(
        Class<E> ce,
        TryF<TryWithResources, S, E> f
    ) {
        var resources = new ArrayList<AutoCloseable>();
        try {
            var res = f.apply(new TryWithResources(resources)); // this may throw...
            var closeErrors = tryCloseAll(resources);

            if (closeErrors.isEmpty()) return Ok(res);
            var firstCloseFailure = closeErrors.getFirst();
            closeErrors.stream()
                .skip(1)
                .forEach(firstCloseFailure::addSuppressed);
            return errorCatchRethrow(ce, new ResultException(firstCloseFailure));
        } catch (ResultException e) {
            var closed
                = tryCloseAll(resources); // ...so we need to close all resources before
            // catchRethrow
            var caught = Result.<S, E>errorCatchRethrow(ce, e);
            closed.forEach(caught.getError()::addSuppressed);
            return caught;
        }
    }

    /**
     * Catch an exception.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import static net.thebugmc.error.Result.*;
     * import java.util.stream.Stream;
     *
     * var result1 = tryRun(Exception.class, r -> {});
     * assertEquals(Ok(), result1);
     *
     * var result2 = tryRun(Exception.class, r -> {
     *     throw new Exception();
     * });
     * assertTrue(result2.isError());
     *
     * var result3 = tryRun(Exception.class, r -> {
     *     var ignored = r.resource(Stream.of(1, 2, 3)).count();
     * });
     * assertEquals(Ok(), result3);
     *}
     *
     * @param ce  Exception type to catch
     * @param r   TryWithResources block to catch
     * @param <E> Error type
     * @return {@link Ok} if successful, {@link Error} if caught an exception
     */
    static <E extends Throwable> Result<Unit, E> tryRun(Class<E> ce, TryC<TryWithResources, E> r) {
        return tryGet(ce, with -> {
            r.accept(with);
            return unit();
        });
    }

    /**
     * Catch an exception.
     *
     * @param ce  Exception type to catch.
     * @param f   TryWithResources block to catch.
     * @param <S> Success type.
     * @param <E> Error type.
     * @return {@link Ok} if successful, {@link Error} if caught an exception.
     * @see #tryGet(Class, TryF)
     */
    static <S, E extends Throwable> Result<S, E> flatTryGet(
        Class<E> ce,
        TryF<TryWithResources, Result<S, E>, E> f
    ) {
        var resources = new ArrayList<AutoCloseable>();
        try {
            var res = f.apply(new TryWithResources(resources)); // this may throw...
            var closeErrors = tryCloseAll(resources);

            if (closeErrors.isEmpty()) return res; // this res may be not ok
            var firstCloseFailure = closeErrors.getFirst();
            closeErrors.stream()
                .skip(1)
                .forEach(firstCloseFailure::addSuppressed);
            return errorCatchRethrow(ce, new ResultException(firstCloseFailure));
        } catch (ResultException e) {
            var closed
                = tryCloseAll(resources); // ...so we need to close all resources before
            // catchRethrow
            var caught = Result.<S, E>errorCatchRethrow(ce, e);
            closed.forEach(caught.getError()::addSuppressed);
            return caught;
        }
    }

    /**
     * Collect all successful results to a list, if possible.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import java.util.List;
     * import java.util.stream.Stream;
     * import net.thebugmc.error.Result;
     * import static net.thebugmc.error.Result.*;
     *
     * var result1 = Stream
     *     .of(Ok(1), Ok(2), Ok(3))
     *     .collect(toResultList());
     * assertEquals(Ok(List.of(1, 2, 3)), result1);
     *
     * var result2 = Stream
     *     .<Result<Integer, String>>of(Ok(1), Error("error"), Ok(3))
     *     .collect(toResultList());
     * assertEquals(Error("error"), result2);
     *}
     *
     * @param <S> Success type.
     * @param <E> Error type.
     * @return Result of collected list or first error.
     * @see #toResultUnit()
     */
    static <S, E> Collector<Result<S, E>, ?, Result<List<S>, E>> toResultList() {
        return Collector.of(
            mapS(
                mapS(
                    ArrayList<S>::new,
                    (Function<List<S>, Result<List<S>, E>>) Result::<List<S>, E>Ok
                ),
                AtomicReference::new
            ),
            (results, newResult) -> results.updateAndGet(r -> r.and(
                newResult,
                peekF2(List::add, ignoreF())
            )),
            (resultsLeft, resultsRight) -> {
                resultsLeft.updateAndGet(r -> r.and(
                    resultsRight.get(),
                    peekF2(List::addAll, ignoreF())
                ));
                return resultsLeft;
            },
            AtomicReference::get
        );
    }

    /**
     * Collect all successful results to a {@link Unit}, if possible.
     *
     * <p>
     * <b>Examples</b>
     * {@snippet :
     * import java.util.List;
     * import java.util.stream.Stream;
     * import net.thebugmc.error.Result;
     * import static net.thebugmc.error.Result.*;
     *
     * var result1 = Stream
     *     .of(Ok(1), Ok("test"), Ok(3.0))
     *     .collect(toResultUnit());
     * assertEquals(Ok(), result1);
     *
     * var result2 = Stream
     *     .<Result<Object, String>>of(Ok(1), Error("error"), Ok(3.0))
     *     .collect(toResultUnit());
     * assertEquals(Error("error"), result2);
     *}
     *
     * @param <E> Error type.
     * @return Result of collected list or first error.
     * @see #toResultList()
     */
    static <E> Collector<Result<?, E>, ?, Result<Unit, E>> toResultUnit() {
        return Collector.of(
            mapS(
                (Supplier<Result<Unit, E>>) Result::Ok,
                AtomicReference::new
            ),
            (results, newResult) -> results.updateAndGet(r -> r.andUnit(newResult)),
            (resultsLeft, resultsRight) -> {
                resultsLeft.updateAndGet(r -> r.flatAndUnit(resultsRight::get));
                return resultsLeft;
            },
            AtomicReference::get
        );
    }

    //
    // Static Functionals
    //

    static <S1, E1, S2> Function<Result<S1, E1>, Result<S2, E1>> applyOk(Function<S1, S2> mapper) {
        return applyF2F(Result::mapOk, mapper);
    }

    static <S1, E1, E2> Function<Result<S1, E1>, Result<S1, E2>> applyError(Function<E1, E2> mapper) {
        return applyF2F(Result::mapError, mapper);
    }

    static <S1, E1, S2, E2> Function<Result<S1, E1>, Result<S2, E2>> apply(
        Function<S1, S2> sMapper,
        Function<E1, E2> eMapper
    ) {
        return result -> result.map(sMapper, eMapper);
    }

    static <S1, E1, S2> Function<Result<S1, E1>, Result<S2, E1>> flatApplyOk(
        Function<S1, Result<S2, E1>> mapper
    ) {
        return result -> switch (result) {
            case Ok(var ok) -> mapper.apply(ok);
            case Error(var error) -> Error(error);
        };
    }

    static <S1, E1, E2> Function<Result<S1, E1>, Result<S1, E2>> flatApplyError(
        Function<E1, Result<S1, E2>> mapper
    ) {
        return result -> switch (result) {
            case Ok(var ok) -> Ok(ok);
            case Error(var error) -> mapper.apply(error);
        };
    }

    static <S1, E1, S2, E2> Function<Result<S1, E1>, Result<S2, E2>> flatApply(
        Function<S1, Result<S2, E2>> sMapper,
        Function<E1, Result<S2, E2>> eMapper
    ) {
        return result -> switch (result) {
            case Ok(var ok) -> sMapper.apply(ok);
            case Error(var error) -> eMapper.apply(error);
        };
    }

    static <S, E> Consumer<Result<S, E>> consumeOk(Consumer<S> success) {
        return applyC2F(Result::peekOk, success);
    }

    static <S, E> Consumer<Result<S, E>> consumeError(Consumer<E> error) {
        return applyC2F(Result::peekError, error);
    }

    static <S, E> Consumer<Result<S, E>> consume(Consumer<S> success, Consumer<E> error) {
        return result -> result.peekOk(success).peekError(error);
    }

    static <S, E> Predicate<Result<S, E>> testOk(Predicate<S> success) {
        return result -> result.ok().filter(success).isPresent();
    }

    static <S, E> Predicate<Result<S, E>> testError(Predicate<E> error) {
        return result -> result.error().filter(error).isPresent();
    }

    static <S, E> Predicate<Result<S, E>> test(Predicate<S> success, Predicate<E> error) {
        return Result.<S, E>testOk(success).or(testError(error));
    }

    //
    // Try
    //

    default <T> S Try(Function<E, T> orReturn) {
        return Functionals.Try(this, orReturn);
    }

    default <T> S Try(Supplier<T> orReturn) {
        return Functionals.Try(this, orReturn);
    }

    default <T> S Try(T orReturn) {
        return Functionals.Try(this, orReturn);
    }

    default S Try() {
        return Functionals.Try(this);
    }
}