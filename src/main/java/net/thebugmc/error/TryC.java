package net.thebugmc.error;

import java.util.function.Consumer;

@FunctionalInterface
public interface TryC<T, E extends Throwable> extends Consumer<T> {
    void tryAccept(T t) throws E;

    @SuppressWarnings("OverlyBroadCatchBlock")
    default void accept(T t) {
        try {
            tryAccept(t);
        } catch (Throwable e) {
            throw new ResultException(e);
        }
    }

    static <T, E extends Throwable> TryC<T, E> of(Consumer<T> f) {
        return f::accept;
    }
}
