package net.thebugmc.error;

import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.function.*;
import java.util.stream.Collector;
import java.util.stream.Stream;

import static java.util.Map.entry;
import static java.util.stream.Collectors.toMap;

/**
 * Utils for {@link Result}, {@link Optional}, {@link Stream} types.
 */
@SuppressWarnings({ "unused", "NewMethodNamingConvention" })
public class Functionals {
    public static void noop() {
    }

    public static <T> Predicate<T> byKey(T key) {
        return byKey(key, f(), Objects::equals);
    }

    public static <T, K> Predicate<T> byKey(K key, Function<T, K> keyExtractor) {
        return byKey(key, keyExtractor, Objects::equals);
    }

    public static <T, K> Predicate<T> byKey(K key, BiPredicate<K, T> keyEquality) {
        return byKey(key, f(), keyEquality);
    }

    public static <T, K1, K2> Predicate<T> byKey(
        K1 key,
        Function<T, K2> keyExtractor,
        BiPredicate<K1, K2> keyEquality
    ) {
        return repeatP(mapP(keyEquality, ignoreS(applyS(key)), keyExtractor));
    }

    //
    // Optional, Stream
    //

    /**
     * @return {@link Optional#of(Object)}
     */
    public static <T> Optional<T> some(T t) {
        return Optional.of(t);
    }

    /**
     * @return {@link Optional#ofNullable(Object)}
     */
    public static <T> Optional<T> optional(T t) {
        return Optional.ofNullable(t);
    }

    /**
     * @return {@link Stream#ofNullable(Object)}
     */
    @SafeVarargs
    public static <T> Stream<T> stream(T... t) {
        return Stream.of(t)
            .filter(Objects::nonNull);
    }

    /**
     * @return {@link Optional#empty()}
     */
    public static <T> Optional<T> none() {
        return Optional.empty();
    }

    /**
     * Convert boolean to optional.
     *
     * @param test   Boolean to convert.
     * @param ifTrue Object to wrap on true test value.
     * @param <T>    Optional type.
     * @return Optional wrapping value if check has passed.
     */
    public static <T> Optional<T> optional(boolean test, Supplier<T> ifTrue) {
        return test ? some(ifTrue.get()) : none();
    }

    /**
     * Convert boolean to optional.
     *
     * @param test   Boolean to convert.
     * @param ifTrue Object to wrap on true test value.
     * @param <T>    Optional type.
     * @return Optional wrapping value if check has passed.
     */
    public static <T> Optional<T> optional(boolean test, T ifTrue) {
        return test ? some(ifTrue) : none();
    }

    public static <A, B> Function<A, Optional<B>> optionalF(Function<A, B> f) {
        return f.andThen(Functionals::optional);
    }

    public static <A, B> Function<A, Stream<B>> streamF(Function<A, Optional<B>> f) {
        return f.andThen(Optional::stream);
    }

    //
    // Reflective Casts
    //

    /**
     * Try to dynamically cast an object to a type.
     * Used for functional style castings in streams and such.
     *
     * @param obj Object to cast.
     * @param <T> Type to cast to.
     * @return Cast type or empty optional.
     */
    public static <T> Optional<T> cast(Object obj, Class<T> c) {
        return Result.tryGet(
                ClassCastException.class,
                TryS.of(applyF2(Class::cast, c, obj))
            )
            .mapOk(Functionals::optional)
            .ok()
            .flatMap(f());
    }

    public static <I, O> Function<I, Optional<O>> cast(Class<O> c) {
        return applyF2F(Functionals::cast, c);
    }

    public static <I, O> Function<I, Stream<O>> castStream(Class<O> c) {
        return Functionals
            .<I, O>cast(c)
            .andThen(Optional::stream);
    }

    //
    // Entry
    //

    public static <K, V> Entry<V, K> flipEntry(Entry<K, V> e) {
        return entry(e.getValue(), e.getKey());
    }

    public static <K, V> Function<K, Entry<K, V>> toEntry(V value) {
        return toEntry(applyS(value));
    }

    public static <K, V> Function<K, Entry<K, V>> toEntry(Supplier<V> f) {
        return toEntry(ignoreS(f));
    }

    public static <K, V> Function<K, Entry<K, V>> toEntry(Function<K, V> f) {
        return repeatF(mapF(Map::entry, f(), f));
    }

    // transpose

    public static <K, V> Optional<Entry<K, V>> transposeEntryKey(Entry<Optional<K>, V> entry) {
        return entry.getKey().map(toEntry(entry.getValue()));
    }

    public static <K, V> Optional<Entry<K, V>> transposeEntryValue(Entry<K, Optional<V>> entry) {
        return transposeEntryKey(flipEntry(entry)).map(Functionals::flipEntry);
    }

    public static <K, V> Optional<Entry<K, V>> transposeEntry(Entry<Optional<K>, Optional<V>> entry) {
        return transposeEntryKey(entry).flatMap(Functionals::transposeEntryValue);
    }

    public static <K, V> Stream<Entry<K, V>> transposeEntryKeyStream(Entry<Stream<K>, V> entry) {
        return entry.getKey().map(toEntry(entry.getValue()));
    }

    public static <K, V> Stream<Entry<K, V>> transposeEntryValueStream(Entry<K, Stream<V>> entry) {
        return transposeEntryKeyStream(flipEntry(entry)).map(Functionals::flipEntry);
    }

    // predicates

    public static <K, V> Predicate<Entry<K, V>> byEntryKey(Predicate<K> keyCheck) {
        return byEntry(ignoreP(keyCheck));
    }

    public static <K, V> Predicate<Entry<K, V>> byEntryValue(Predicate<V> valueCheck) {
        return byEntry(flipP(ignoreP(valueCheck)));
    }

    public static <K, V> Predicate<Entry<K, V>> byEntry(BiPredicate<K, V> entryCheck) {
        return repeatP(mapP(entryCheck, Entry::getKey, Entry::getValue));
    }

    // map

    public static <K, V, N> Entry<N, V> mapEntryKey(Entry<K, V> e, Function<K, N> f) {
        return entry(f.apply(e.getKey()), e.getValue());
    }

    public static <K, V, N> Entry<K, N> mapEntryValue(Entry<K, V> e, Function<V, N> f) {
        return entry(e.getKey(), f.apply(e.getValue()));
    }

    public static <K, V, N> N mapEntry(Entry<K, V> e, BiFunction<K, V, N> f) {
        return f.apply(e.getKey(), e.getValue());
    }

    public static <K, V, X, Y> Entry<X, Y> mapEntry(
        Entry<K, V> e,
        Function<K, X> f1,
        Function<V, Y> f2
    ) {
        return mapEntryValue(mapEntryKey(e, f1), f2);
    }

    public static <K, V, N> Function<Entry<K, V>, Entry<N, V>> mapEntryKey(Function<K, N> f) {
        return applyF2F(Functionals::mapEntryKey, f);
    }

    public static <K, V, N> Function<Entry<K, V>, Entry<K, N>> mapEntryValue(Function<V, N> f) {
        return applyF2F(Functionals::mapEntryValue, f);
    }

    public static <K, V, N> Function<Entry<K, V>, N> mapEntry(BiFunction<K, V, N> f) {
        return applyF2F(Functionals::mapEntry, f);
    }

    public static <K, V, X, Y> Function<Entry<K, V>, Entry<X, Y>> mapEntry(
        Function<K, X> f1,
        Function<V, Y> f2
    ) {
        return Functionals.<K, V, X>mapEntryKey(f1).andThen(mapEntryValue(f2));
    }

    // consumers

    public static <K, V> Consumer<Entry<K, V>> forEntryKey(Consumer<K> c) {
        return forEntry(ignoreC(c));
    }

    public static <K, V> Consumer<Entry<K, V>> forEntryValue(Consumer<V> c) {
        return forEntry(flipC(ignoreC(c)));
    }

    public static <K, V> Consumer<Entry<K, V>> forEntry(BiConsumer<K, V> c) {
        return repeatC(mapC(c, Entry::getKey, Entry::getValue));
    }

    // suppliers

    public static <K, V> Supplier<Entry<K, V>> ofEntry(K k, V v) {
        return applyS(entry(k, v));
    }

    public static <K, V> Supplier<Entry<K, V>> ofEntry(K k, Supplier<V> v) {
        return ofEntry(applyS(k), v);
    }

    public static <K, V> Supplier<Entry<K, V>> ofEntry(Supplier<K> k, V v) {
        return ofEntry(k, applyS(v));
    }

    public static <K, V> Supplier<Entry<K, V>> ofEntry(Supplier<K> k, Supplier<V> v) {
        return () -> entry(k.get(), v.get());
    }

    // collectors

    public static <K, V> Collector<Entry<K, V>, ?, Map<K, V>> toEntryMap() {
        return toMap(
            Entry::getKey,
            Entry::getValue
        );
    }

    public static <K, V, M extends Map<K, V>> Collector<Entry<K, V>, ?, M> toEntryMap(Supplier<M> map) {
        return toMap(
            Entry::getKey,
            Entry::getValue,
            Functionals.<V, V>ignoreF()::apply,
            map
        );
    }

    //
    // Lambda Conversion (keeping the parameter count)
    //

    // BiP -> BiP
    //   P -> P
    // P,P -> BiP

    public static <A, B> BiPredicate<A, B> not(BiPredicate<A, B> f) {
        return f.negate();
    }

    public static <T> Predicate<T> not(Predicate<T> f) {
        return f.negate();
    }

    public static <A, B> BiPredicate<A, B> and(Predicate<A> fa, Predicate<B> fb) {
        return (a, b) -> fa.test(a) && fb.test(b);
    }

    public static <A, B> BiPredicate<A, B> or(Predicate<A> fa, Predicate<B> fb) {
        return (a, b) -> fa.test(a) || fb.test(b);
    }

    // Lambda quick cast

    public static <T extends R, R> Function<T, R> f() {
        return t -> t;
    }

    public static <T, R> Function<T, R> f(Function<T, R> f) {
        return f;
    }

    public static <A, B, R> BiFunction<A, B, R> f2(BiFunction<A, B, R> f) {
        return f;
    }

    public static Predicate<Boolean> p() {
        return Functionals.<Boolean, Boolean>f()::apply;
    }

    public static <T> Predicate<T> p(Predicate<T> f) {
        return f;
    }

    public static <A, B> BiPredicate<A, B> p2(BiPredicate<A, B> f) {
        return f;
    }

    public static <T> Consumer<T> c() {
        return ignoreR();
    }

    public static <T> Consumer<T> c(Consumer<T> f) {
        return f;
    }

    public static <A, B> BiConsumer<A, B> c2(BiConsumer<A, B> f) {
        return f;
    }

    public static <T> Supplier<T> s(Supplier<T> f) {
        return f;
    }

    public static Runnable r() {
        return Functionals::noop;
    }

    public static Runnable r(Runnable f) {
        return f;
    }

    // Other

    public static <T> T map(T t, Predicate<T> check, Function<T, T> ifTrue) {
        return map(t, check, ifTrue, f());
    }

    public static <T> T map(
        T t, Predicate<T> check,
        Function<T, T> ifTrue,
        Function<T, T> ifFalse
    ) {
        return (check.test(t) ? ifTrue : ifFalse).apply(t);
    }

    // Flip

    // BiF(A, B)->BiF(B, A)
    // BiP(A, B)->BiP(B, A)
    // BiC(A, B)->BiC(B, A)

    public static <A, B, R> BiFunction<B, A, R> flipF(BiFunction<A, B, R> f) {
        return (b, a) -> f.apply(a, b);
    }

    public static <A, B> BiPredicate<B, A> flipP(BiPredicate<A, B> f) {
        return flipF(f::test)::apply;
    }

    public static <A, B> BiConsumer<B, A> flipC(BiConsumer<A, B> f) {
        return (b, a) -> f.accept(a, b);
    }

    //
    // Partial Application (lower the parameter count by supplying them)
    //

    // Function

    // BiF(A, B)->R -> F(B)->R
    // BiF(A, B)->R -> F(A)->R
    // BiF(A, B)->R -> S()->R
    //      F(T)->R -> S()->R

    public static <A, B, R> Function<B, R> applyF2(BiFunction<A, B, R> f, A a) {
        return b -> f.apply(a, b);
    }

    public static <A, B, R> Function<A, R> applyF2F(BiFunction<A, B, R> f, B b) {
        return applyF2(flipF(f), b);
    }

    public static <A, B, R> Supplier<R> applyF2(BiFunction<A, B, R> f, A a, B b) {
        return () -> f.apply(a, b);
    }

    public static <T, R> Supplier<R> applyF(Function<T, R> f, T t) {
        return () -> f.apply(t);
    }

    //      BiF(S()->A, B)->R -> F(B)->R
    //      BiF(A, S()->B)->R -> F(A)->R
    // BiF(S()->A, S()->B)->R -> S()->R
    //           F(S()->T)->R -> S()->R

    public static <A, B, R> Function<B, R> applyF2S(BiFunction<A, B, R> f, Supplier<A> a) {
        return b -> f.apply(a.get(), b);
    }

    public static <A, B, R> Function<A, R> applyF2SF(BiFunction<A, B, R> f, Supplier<B> b) {
        return applyF2S(flipF(f), b);
    }

    public static <A, B, R> Supplier<R> applyF2S(
        BiFunction<A, B, R> f,
        Supplier<A> a,
        Supplier<B> b
    ) {
        return () -> f.apply(a.get(), b.get());
    }

    public static <T, R> Supplier<R> applyFS(Function<T, R> f, Supplier<T> t) {
        return () -> f.apply(t.get());
    }

    // Predicate

    // BiP(A, B) -> P(B)
    // BiP(A, B) -> P(A)
    // BiP(A, B) -> S()->bool
    //      P(T) -> S()->bool

    public static <A, B> Predicate<B> applyP2(BiPredicate<A, B> f, A a) {
        return applyF2(f::test, a)::apply;
    }

    public static <A, B> Predicate<A> applyP2F(BiPredicate<A, B> f, B b) {
        return applyF2F(f::test, b)::apply;
    }

    public static <A, B> BooleanSupplier applyP2(BiPredicate<A, B> f, A a, B b) {
        return applyF2(f::test, a, b)::get;
    }

    public static <T> BooleanSupplier applyP(Predicate<T> f, T t) {
        return applyF(f::test, t)::get;
    }

    //      BiP(S()->A, B) -> P(B)
    //      BiP(A, S()->B) -> P(A)
    // BiP(S()->A, S()->B) -> S()->bool
    //           P(S()->T) -> S()->bool

    public static <A, B> Predicate<B> applyP2S(BiPredicate<A, B> f, Supplier<A> a) {
        return applyF2S(f::test, a)::apply;
    }

    public static <A, B> Predicate<A> applyP2SF(BiPredicate<A, B> f, Supplier<B> b) {
        return applyF2SF(f::test, b)::apply;
    }

    public static <A, B> BooleanSupplier applyP2S(
        BiPredicate<A, B> f,
        Supplier<A> a,
        Supplier<B> b
    ) {
        return applyF2S(f::test, a, b)::get;
    }

    public static <T> BooleanSupplier applyPS(Predicate<T> f, Supplier<T> t) {
        return applyFS(f::test, t)::get;
    }

    // Consumer

    // BiC(A, B) -> C(A)
    // BiC(A, B) -> C(B)
    // BiC(A, B) -> R()
    //      C(T) -> R()

    public static <A, B> Consumer<B> applyC2(BiConsumer<A, B> f, A a) {
        return b -> f.accept(a, b);
    }

    public static <A, B> Consumer<A> applyC2F(BiConsumer<A, B> f, B b) {
        return applyC2(flipC(f), b);
    }

    public static <A, B> Runnable applyC2(BiConsumer<A, B> f, A a, B b) {
        return () -> f.accept(a, b);
    }

    public static <T> Runnable applyC(Consumer<T> f, T t) {
        return () -> f.accept(t);
    }

    //      BiC(S()->A, B) -> C(A)
    //      BiC(A, S()->B) -> C(B)
    // BiC(S()->A, S()->B) -> R()
    //           C(S()->T) -> R()

    public static <A, B> Consumer<B> applyC2S(BiConsumer<A, B> f, Supplier<A> a) {
        return b -> f.accept(a.get(), b);
    }

    public static <A, B> Consumer<A> applyC2SF(BiConsumer<A, B> f, Supplier<B> b) {
        return applyC2S(flipC(f), b);
    }

    public static <A, B> Runnable applyC2S(BiConsumer<A, B> f, Supplier<A> a, Supplier<B> b) {
        return () -> f.accept(a.get(), b.get());
    }

    public static <T> Runnable applyCS(Consumer<T> f, Supplier<T> t) {
        return () -> f.accept(t.get());
    }

    // Supplier

    // T -> S()->T

    public static <T> Supplier<T> applyS(T t) {
        return () -> t;
    }

    //
    // Parameter Ignores (raise the parameter count by ignoring them)
    //

    // Function

    // F(A)->R -> BiF(A, B)->R

    public static <A, B, R> BiFunction<A, B, R> ignoreF(Function<A, R> f) {
        return (a, b) -> f.apply(a);
    }

    public static <A, B> BiFunction<A, B, A> ignoreF() {
        return ignoreF(f());
    }

    // Predicate

    // P(A) -> BiP(A, B)

    public static <A, B> BiPredicate<A, B> ignoreP(Predicate<A> f) {
        return ignoreF(f::test)::apply;
    }

    // Consumer

    // C(A) -> BiC(A, B)

    public static <A, B> BiConsumer<A, B> ignoreC(Consumer<A> f) {
        return (a, b) -> f.accept(a);
    }

    public static <A, B> BiConsumer<A, B> ignoreC() {
        return ignoreC(c());
    }

    // Supplier

    // S()->R -> BiF(A, B)->R
    // S()->R -> F(T)->R

    public static <A, B, R> BiFunction<A, B, R> ignoreS2(Supplier<R> f) {
        return (a, b) -> f.get();
    }

    public static <T, R> Function<T, R> ignoreS(Supplier<R> f) {
        return t -> f.get();
    }

    // Runnable

    // R() -> BiC(A, B)
    // R() -> C(T)

    public static <A, B> BiConsumer<A, B> ignoreR2(Runnable f) {
        return (a, b) -> f.run();
    }

    public static <A, B> BiConsumer<A, B> ignoreR2() {
        return ignoreR2(r());
    }

    public static <T> Consumer<T> ignoreR(Runnable f) {
        return t -> f.run();
    }

    public static <T> Consumer<T> ignoreR() {
        return ignoreR(r());
    }

    //
    // Parameter Repeating (lower the parameter count by repeating them)
    //

    // Function

    public static <T, R> Function<T, R> repeatF(BiFunction<T, T, R> f) {
        return t -> f.apply(t, t);
    }

    // Predicate

    public static <T> Predicate<T> repeatP(BiPredicate<T, T> f) {
        return repeatF(f::test)::apply;
    }

    // Consumer

    public static <T> Consumer<T> repeatC(BiConsumer<T, T> f) {
        return t -> f.accept(t, t);
    }

    //
    // Mapping (keeping the parameter count, but converting input types)
    //

    // Function

    public static <A, B, N, M, R> BiFunction<N, M, R> mapF(
        BiFunction<A, B, R> f,
        Function<N, A> na,
        Function<M, B> mb
    ) {
        return (n, m) -> f.apply(na.apply(n), mb.apply(m));
    }

    public static <T, N, R> Function<N, R> mapF(
        Function<T, R> f,
        Function<N, T> na
    ) {
        return f.compose(na);
    }

    // Predicate

    public static <A, B, N, M> BiPredicate<N, M> mapP(
        BiPredicate<A, B> f,
        Function<N, A> na,
        Function<M, B> mb
    ) {
        return mapF(f::test, na, mb)::apply;
    }

    public static <T, N> Predicate<N> mapP(
        Predicate<T> f,
        Function<N, T> na
    ) {
        return mapF(f::test, na)::apply;
    }

    // Consumer

    public static <A, B, N, M> BiConsumer<N, M> mapC(
        BiConsumer<A, B> f,
        Function<N, A> na,
        Function<M, B> mb
    ) {
        return (n, m) -> f.accept(na.apply(n), mb.apply(m));
    }

    public static <T, N> Consumer<N> mapC(
        Consumer<T> f,
        Function<N, T> na
    ) {
        return n -> f.accept(na.apply(n));
    }

    // Supplier

    public static <T, N> Supplier<N> mapS(
        Supplier<T> f,
        Function<T, N> na
    ) {
        return applyFS(na, f);
    }

    //
    // Peeking (keeping the parameter count, but performing a side effect action)
    //

    // Function

    public static <A, B, R> BiFunction<A, B, R> peekF2(
        BiConsumer<A, B> action,
        BiFunction<A, B, R> f
    ) {
        return (a, b) -> {
            action.accept(a, b);
            return f.apply(a, b);
        };
    }

    public static <T, R> Function<T, R> peekF(Consumer<T> action, Function<T, R> f) {
        return t -> {
            action.accept(t);
            return f.apply(t);
        };
    }

    public static <A, B, R> BiFunction<A, B, R> peekF2O(Consumer<R> action, BiFunction<A, B, R> f) {
        return (a, b) -> {
            var r = f.apply(a, b);
            action.accept(r);
            return r;
        };
    }

    public static <T, R> Function<T, R> peekFO(Consumer<R> action, Function<T, R> f) {
        return t -> {
            var r = f.apply(t);
            action.accept(r);
            return r;
        };
    }

    // Predicate

    public static <A, B> BiPredicate<A, B> peekP2(BiConsumer<A, B> action, BiPredicate<A, B> f) {
        return peekF2(action, f::test)::apply;
    }

    public static <T> Predicate<T> peekP(Consumer<T> action, Predicate<T> f) {
        return peekF(action, f::test)::apply;
    }

    // Consumer

    public static <A, B> BiConsumer<A, B> peekC2(BiConsumer<A, B> action, BiConsumer<A, B> f) {
        return action.andThen(f);
    }

    public static <T> Consumer<T> peekC(Consumer<T> action, Consumer<T> f) {
        return action.andThen(f);
    }

    public static <A, B> BiConsumer<A, B> peekC2O(BiConsumer<A, B> action, BiConsumer<A, B> f) {
        return f.andThen(action);
    }

    public static <T> Consumer<T> peekCO(Consumer<T> action, Consumer<T> f) {
        return f.andThen(action);
    }

    // Supplier

    public static <T> Supplier<T> peekSO(Consumer<T> action, Supplier<T> f) {
        return () -> {
            var t = f.get();
            action.accept(t);
            return t;
        };
    }

    // Runnable

    public static Runnable peekR(Runnable action, Runnable f) {
        return () -> {
            action.run();
            f.run();
        };
    }

    public static Runnable peekRO(Runnable action, Runnable f) {
        return () -> {
            f.run();
            action.run();
        };
    }

    //
    // Higher Order Partial Application
    //

    // Function

    public static <T, R> Function<Function<T, R>, R> flatApplyF(T t) {
        return applyF2F(Function::apply, t);
    }

    // Predicate

    public static <T> Predicate<Predicate<T>> flatApplyP(T t) {
        return applyP2F(Predicate::test, t);
    }

    // Consumer

    public static <T> Consumer<Consumer<T>> flatApplyC(T t) {
        return applyC2F(Consumer::accept, t);
    }

    // Supplier

    public static <T> Supplier<Supplier<T>> flatApplyS(T t) {
        return applyS(applyS(t));
    }

    //
    // Propagation
    //

    public static boolean get(boolean res) {
        if (!res) throw new NoSuchElementException("Cannot unwrap error");
        return true;
    }

    public static <T> T get(Optional<T> res) {
        return res.get();
    }

    public static <T> T get(Result<T, ?> res) {
        return res.get();
    }

    public static boolean isError(boolean res) {
        return !res;
    }

    public static boolean isError(Optional<?> res) {
        return res.isEmpty();
    }

    public static boolean isError(Result<?, ?> res) {
        return res.isError();
    }

    public static boolean propagate(boolean res) {
        return false; // default propagation assumed false
    }

    public static <T2> Optional<T2> propagate(Optional<?> opt) {
        return none(); // default propagation assumed none()
    }

    public static <S2, E> Result<S2, E> propagate(Result<?, E> res) {
        return Result.Error(res.error().get());
    }

    public static <E2, E> E2 propagate(boolean res, Supplier<E2> orReturn) {
        return orReturn.get();
    }

    public static <E2, E> E2 propagate(boolean res, E2 orReturn) {
        return orReturn;
    }

    public static <E2, E> E2 propagate(Optional<?> res, Supplier<E2> orReturn) {
        return orReturn.get();
    }

    public static <E2, E> E2 propagate(Optional<?> res, E2 orReturn) {
        return orReturn;
    }

    public static <E2, E> E2 propagate(Result<?, E> res, Function<E, E2> orReturn) {
        return orReturn.apply(res.error().get());
    }

    public static <E2, E> E2 propagate(Result<?, E> res, Supplier<E2> orReturn) {
        return orReturn.get();
    }

    public static <E2, E> E2 propagate(Result<?, E> res, E2 orReturn) {
        return orReturn;
    }

    public static <S, E, T> boolean Try(boolean res, Supplier<T> orReturn) {
        if (isError(res)) /* return */ propagate(false, orReturn);
        return get(res);
    }

    public static <S, E, T> boolean Try(boolean res, T orReturn) {
        if (isError(res)) /* return */ propagate(false, orReturn);
        return get(res);
    }

    public static <S, E> boolean Try(boolean res) {
        if (isError(res)) /* return */ propagate(false);
        return get(res);
    }

    public static <S, E, T> S Try(Optional<S> res, Supplier<T> orReturn) {
        if (isError(res)) /* return */ propagate(res, orReturn);
        return get(res);
    }

    public static <S, E, T> S Try(Optional<S> res, T orReturn) {
        if (isError(res)) /* return */ propagate(res, orReturn);
        return get(res);
    }

    public static <S, E> S Try(Optional<S> res) {
        if (isError(res)) /* return */ propagate(res);
        return get(res);
    }

    public static <S, E, T> S Try(Result<S, E> res, Function<E, T> orReturn) {
        if (isError(res)) /* return */ propagate(res, orReturn);
        return get(res);
    }

    public static <S, E, T> S Try(Result<S, E> res, Supplier<T> orReturn) {
        if (isError(res)) /* return */ propagate(res, orReturn);
        return get(res);
    }

    public static <S, E, T> S Try(Result<S, E> res, T orReturn) {
        if (isError(res)) /* return */ propagate(res, orReturn);
        return get(res);
    }

    public static <S, E> S Try(Result<S, E> res) {
        if (isError(res)) /* return */ propagate(res);
        return get(res);
    }

    public static <S, T> S TryCast(S res, Supplier<T> orReturn) {
        return res;
    }

    public static <S, T> S TryCast(S res, T orReturn) {
        return res;
    }

    public static <S, T> S TryCast(S res) {
        return res;
    }
}