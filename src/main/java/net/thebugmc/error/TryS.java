package net.thebugmc.error;

import java.util.function.Supplier;

@FunctionalInterface
public interface TryS<T, E extends Throwable> extends Supplier<T> {
    T tryGet() throws E;

    @SuppressWarnings("OverlyBroadCatchBlock")
    default T get() {
        try {
            return tryGet();
        } catch (Throwable e) {
            throw new ResultException(e);
        }
    }

    static <T, E extends Throwable> TryS<T, E> of(Supplier<T> f) {
        return f::get;
    }
}
