package net.thebugmc.error;

import java.util.function.BiConsumer;

@FunctionalInterface
public interface TryBiC<T, U, E extends Throwable> extends BiConsumer<T, U> {
    void tryAccept(T t, U u) throws E;

    @SuppressWarnings("OverlyBroadCatchBlock")
    default void accept(T t, U u) {
        try {
            tryAccept(t, u);
        } catch (Throwable e) {
            throw new ResultException(e);
        }
    }

    static <T, U, E extends Throwable> TryBiC<T, U, E> of(BiConsumer<T, U> f) {
        return f::accept;
    }
}
