package net.thebugmc.error;

/**
 * Analog of Scala's / Kotlin's {@code Unit} or Rust's {@code ()} type.
 */
public final class Unit {
    private static final Unit INSTANCE = new Unit();

    public static Unit unit() {
        return INSTANCE;
    }

    private Unit() {
    }

    public void toVoid() {
    }

    public String toString() {
        return "Unit";
    }
}