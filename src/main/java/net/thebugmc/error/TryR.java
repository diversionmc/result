package net.thebugmc.error;

@FunctionalInterface
public interface TryR<E extends Throwable> extends Runnable {
    void tryRun() throws E;

    @SuppressWarnings("OverlyBroadCatchBlock")
    default void run() {
        try {
            tryRun();
        } catch (Throwable e) {
            throw new ResultException(e);
        }
    }

    static <E extends Throwable> TryR<E> of(Runnable f) {
        return f::run;
    }
}
