package net.thebugmc.error;

import java.util.function.Function;

@FunctionalInterface
public interface TryF<T, R, E extends Throwable> extends Function<T, R> {
    R tryApply(T t) throws E;

    @SuppressWarnings("OverlyBroadCatchBlock")
    default R apply(T t) {
        try {
            return tryApply(t);
        } catch (Throwable e) {
            throw new ResultException(e);
        }
    }

    static <T, R, E extends Throwable> TryF<T, R, E> of(Function<T, R> f) {
        return f::apply;
    }
}
