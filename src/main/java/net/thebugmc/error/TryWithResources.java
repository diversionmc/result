package net.thebugmc.error;

import java.util.Collection;

/**
 * Write-only wrapper for a backed-up resource collection.
 *
 * <p>
 * In {@link Result#tryGet(Class, TryF)} / {@link Result#tryRun(Class, TryC)} only writing to the
 * collection is allowed, while the backed up collection is accessed in those methods.
 */
public class TryWithResources {
    private final Collection<AutoCloseable> backup;

    public TryWithResources(Collection<AutoCloseable> backup) {
        this.backup = backup;
    }

    public <R extends AutoCloseable> R resource(R resource) {
        backup.add(resource);
        return resource;
    }
}