package net.thebugmc.error;

import java.util.function.BiFunction;

@FunctionalInterface
public interface TryBiF<T, U, R, E extends Throwable> extends BiFunction<T, U, R> {
    R tryApply(T t, U u) throws E;

    @SuppressWarnings("OverlyBroadCatchBlock")
    default R apply(T t, U u) {
        try {
            return tryApply(t, u);
        } catch (Throwable e) {
            throw new ResultException(e);
        }
    }

    static <T, U, R, E extends Throwable> TryBiF<T, U, R, E> of(BiFunction<T, U, R> f) {
        return f::apply;
    }
}
