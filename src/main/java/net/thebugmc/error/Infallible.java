package net.thebugmc.error;

import static net.thebugmc.error.UnreachableError.unreachable;

/**
 * An exception that can never be thrown. Usually used as an error generic parameter, and usually
 * when that has to extend {@link Throwable} or {@link Exception}.
 */
public final class Infallible extends RuntimeException {
    private Infallible() {
        throw unreachable("tried to create an `Infallible`");
    }
}
