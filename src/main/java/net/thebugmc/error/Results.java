package net.thebugmc.error;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;
import java.util.stream.Collector;

import static java.util.Collections.unmodifiableList;
import static net.thebugmc.error.Functionals.applyF;
import static net.thebugmc.error.Functionals.mapF;
import static net.thebugmc.error.Result.Ok;
import static net.thebugmc.error.Result.tryGet;

/**
 * Results represents collected oks and errors from a {@link Result} stream.
 *
 * @param <S> Success type
 * @param <E> Error type
 */
public class Results<S, E> {
    private final List<S> oks = new ArrayList<>();
    private final List<E> errors = new ArrayList<>();

    /**
     * Immutably combine two results into one.
     *
     * @param other Other results to combine with this.
     * @return New collected results.
     */
    public Results<S, E> combine(Results<S, E> other) {
        var res = new Results<S, E>();
        res.oks.addAll(oks);
        res.oks.addAll(other.oks);
        res.errors.addAll(errors);
        res.errors.addAll(other.errors);
        return res;
    }

    /**
     * Get all successful results values.
     *
     * @return List of oks.
     */
    public List<S> oks() {
        return unmodifiableList(oks);
    }

    /**
     * Get all unsuccessful results values.
     *
     * @return List of errors.
     */
    public List<E> errors() {
        return unmodifiableList(errors);
    }

    /**
     * Displays all successful and unsuccessful values.
     *
     * @return {oks, errors}
     */
    public String toString() {
        return "{%s, %s}".formatted(oks, errors);
    }

    /**
     * Collect results to two lists: successful results, and error results.
     *
     * @param <S> Success type.
     * @param <E> Error type.
     * @return Separated results into oks and errors.
     */
    public static <S, E> Collector<Result<S, E>, ?, Results<S, E>> toResults() {
        return Collector.of(
            Results::new,
            (results, result) -> result
                .peekOk(results.oks::add)
                .peekError(results.errors::add),
            Results::combine
        );
    }

    //
    // Static utils
    //

    /**
     * A public version of {@code NumberFormatException#forInputString(String, int)}.
     */
    public static NumberFormatException forInputString(String s) {
        return new NumberFormatException("For input string: \"" + s + "\"");
    }

    /**
     * Try to parse a boolean.
     *
     * @param s String to parse.
     * @return Result of {@link Boolean#parseBoolean(String)}.
     */
    public static Result<Boolean, NumberFormatException> parseBoolean(String s) {
        return Ok(Boolean.parseBoolean(s));
    }

    /**
     * Try to parse a byte.
     *
     * @param s String to parse.
     * @return Result of {@link Byte#parseByte(String)}.
     */
    public static Result<Byte, NumberFormatException> parseByte(String s) {
        return tryGet(
            NumberFormatException.class,
            TryS.of(applyF(Byte::parseByte, s))
        );
    }

    /**
     * Try to parse an unsigned byte.
     *
     * @param s String to parse.
     * @return {@link Integer#parseUnsignedInt(String)} with a range check.
     */
    public static Result<Byte, NumberFormatException> parseUnsignedByte(String s) {
        return tryGet(
            NumberFormatException.class,
            TryS.of(applyF(
                mapF(
                    i -> {
                        if (i > 255) throw forInputString(s);
                        return i.byteValue();
                    },
                    Integer::parseUnsignedInt
                ),
                s
            ))
        );
    }

    /**
     * Try to parse a short.
     *
     * @param s String to parse.
     * @return Result of {@link Short#parseShort(String)}.
     */
    public static Result<Short, NumberFormatException> parseShort(String s) {
        return tryGet(
            NumberFormatException.class,
            TryS.of(applyF(Short::parseShort, s))
        );
    }

    /**
     * Try to parse an unsigned short.
     *
     * @param s String to parse.
     * @return {@link Integer#parseUnsignedInt(String)} with a range check.
     */
    public static Result<Short, NumberFormatException> parseUnsignedShort(String s) {
        return tryGet(
            NumberFormatException.class,
            TryS.of(applyF(
                mapF(
                    i -> {
                        if (i > 65535) throw forInputString(s);
                        return i.shortValue();
                    },
                    Integer::parseUnsignedInt
                ),
                s
            ))
        );
    }

    /**
     * Try to parse a character.
     *
     * @param s String to parse.
     * @return Result of charAt via length 1 string.
     */
    public static Result<Character, NumberFormatException> parseCharacter(String s) {
        return Result.check(
            s.length() == 1,
            () -> s.charAt(0),
            () -> new NumberFormatException("For input string: \"%s\"".formatted(s))
        );
    }

    /**
     * Try to parse a int.
     *
     * @param s String to parse.
     * @return Result of {@link Integer#parseInt(String)}.
     */
    public static Result<Integer, NumberFormatException> parseInt(String s) {
        return tryGet(
            NumberFormatException.class,
            TryS.of(applyF(Integer::parseInt, s))
        );
    }

    /**
     * Try to parse an unsigned int.
     *
     * @param s String to parse.
     * @return Result of {@link Integer#parseUnsignedInt(String)}.
     */
    public static Result<Integer, NumberFormatException> parseUnsignedInt(String s) {
        return tryGet(
            NumberFormatException.class,
            TryS.of(applyF(Integer::parseUnsignedInt, s))
        );
    }

    /**
     * Try to parse a long.
     *
     * @param s String to parse.
     * @return Result of {@link Long#parseLong(String)}.
     */
    public static Result<Long, NumberFormatException> parseLong(String s) {
        return tryGet(
            NumberFormatException.class,
            TryS.of(applyF(Long::parseLong, s))
        );
    }

    /**
     * Try to parse an unsigned long.
     *
     * @param s String to parse.
     * @return Result of {@link Long#parseUnsignedLong(String)}.
     */
    public static Result<Long, NumberFormatException> parseUnsignedLong(String s) {
        return tryGet(
            NumberFormatException.class,
            TryS.of(applyF(Long::parseUnsignedLong, s))
        );
    }

    /**
     * Try to parse a float.
     *
     * @param s String to parse.
     * @return Result of {@link Float#parseFloat(String)}.
     */
    public static Result<Float, NumberFormatException> parseFloat(String s) {
        return tryGet(
            NumberFormatException.class,
            TryS.of(applyF(Float::parseFloat, s))
        );
    }

    /**
     * Try to parse a double.
     *
     * @param s String to parse.
     * @return Result of {@link Double#parseDouble(String)}.
     */
    public static Result<Double, NumberFormatException> parseDouble(String s) {
        return tryGet(
            NumberFormatException.class,
            TryS.of(applyF(Double::parseDouble, s))
        );
    }

    private static final int UUID_TRIMMED_LENGTH = 32;
    private static final Pattern STRING_PATTERN
        = Pattern.compile("(.{8})(.{4})(.{4})(.{4})(.{12})");

    /**
     * Try to parse a UUID.
     *
     * @param s UUID to parse.
     * @return Result of {@link UUID#fromString(String)}
     * + attempt to parse a trimmed (dash-less) UUID.
     */
    public static Result<UUID, IllegalArgumentException> parseUUID(String s) {
        return tryGet(
            IllegalArgumentException.class,
            TryS.of(applyF(
                UUID::fromString,
                s.length() == UUID_TRIMMED_LENGTH
                    ? STRING_PATTERN
                    .matcher(s)
                    .replaceAll("$1-$2-$3-$4-$5")
                    : s
            ))
        );
    }
}