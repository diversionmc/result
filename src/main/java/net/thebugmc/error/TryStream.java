package net.thebugmc.error;

import java.util.*;
import java.util.function.*;
import java.util.stream.*;
import java.util.stream.Collector.Characteristics;

/**
 * An implementation of {@link Stream} that allows you to use try-predicates in its terminal
 * operations, which enables you to perform throwing operations in them without using {@code try}.
 *
 * <p>
 * Interfacing with {@link IntStream}, {@link LongStream}, and {@link DoubleStream} is not
 * supported.
 *
 * @param <T> The type of the elements
 */
@SuppressWarnings({"NullableProblems", "RedundantSuppression", "unused"})
public final class TryStream<T> implements Stream<T> {
    private final Stream<T> stream;

    public TryStream(Stream<T> stream) {
        this.stream = stream;
    }

    public static <T> TryStream<T> empty() {
        return new TryStream<>(Stream.of());
    }

    @SafeVarargs
    public static <T> TryStream<T> of(T... values) {
        return new TryStream<>(Stream.of(values));
    }

    public static <T> TryStream<T> of(Iterable<T> values) {
        return new TryStream<>(StreamSupport.stream(values.spliterator(), false));
    }

    public static <T> TryStream<T> of(Collection<T> values) {
        return new TryStream<>(values.stream());
    }

    public static <T> TryStream<T> of(Stream<T> values) {
        return new TryStream<>(values);
    }

    //
    // Custom intermediate operations
    //

    public <E extends Throwable> TryStream<T> filter(TryP<? super T, E> predicate) {
        return new TryStream<>(stream.filter(predicate));
    }

    public <R, E extends Throwable> TryStream<R> map(TryF<? super T, ? extends R, E> mapper) {
        return new TryStream<>(stream.map(mapper));
    }

    public <R, E extends Throwable> TryStream<R> flatMap(
        TryF<? super T, ? extends Stream<? extends R>, E> mapper
    ) {
        return new TryStream<>(stream.flatMap(mapper));
    }

    public <E extends Throwable> TryStream<T> onClose(TryR<E> closeHandler) {
        return new TryStream<>(stream.onClose(closeHandler));
    }

    //
    // Custom terminal operations
    //

    /**
     * <b>Terminal Operations Note</b>
     * <p>
     * Every terminal operation in {@link TryStream} can accept an exception class. Whenever a
     * throw happens anywhere in the stream chain using the trying functional interfaces, the
     * exception will be caught and rethrown as an {@link E}.
     */
    @SuppressWarnings("unchecked")
    public <E extends Throwable> void forEach(Class<E> ce, TryC<? super T, E> action) throws E {
        try {
            stream.forEach(action);
        } catch (ResultException e) {
            var ex = e.getCause();
            if (ce.isInstance(ex))
                throw (E) ex;
            throw e;
        }
    }

    /**
     * <b>Terminal Operations Note</b>
     * <p>
     * Every terminal operation in {@link TryStream} can accept an exception class. Whenever a
     * throw happens anywhere in the stream chain using the trying functional interfaces, the
     * exception will be caught and rethrown as an {@link E}.
     */
    @SuppressWarnings("unchecked")
    public <E extends Throwable> void forEachOrdered(Class<E> ce, TryC<? super T, E> action)
    throws E {
        try {
            stream.forEachOrdered(action);
        } catch (ResultException e) {
            var ex = e.getCause();
            if (ce.isInstance(ex))
                throw (E) ex;
            throw e;
        }
    }

    /**
     * <b>Terminal Operations Note</b>
     * <p>
     * Every terminal operation in {@link TryStream} can accept an exception class. Whenever a
     * throw happens anywhere in the stream chain using the trying functional interfaces, the
     * exception will be caught and rethrown as an {@link E}.
     */
    @SuppressWarnings({"unchecked", "UnreachableCode"})
    public <E extends Throwable> Object[] toArray(Class<E> ce) throws E {
        try {
            return stream.toArray();
        } catch (ResultException e) {
            var ex = e.getCause();
            if (ce.isInstance(ex))
                throw (E) ex;
            throw e;
        }
    }

    /**
     * <b>Terminal Operations Note</b>
     * <p>
     * Every terminal operation in {@link TryStream} can accept an exception class. Whenever a
     * throw happens anywhere in the stream chain using the trying functional interfaces, the
     * exception will be caught and rethrown as an {@link E}.
     */
    @SuppressWarnings("unchecked")
    public <A, E extends Throwable> A[] toArray(Class<E> ce, IntFunction<A[]> generator) throws E {
        try {
            return stream.toArray(generator);
        } catch (ResultException e) {
            var ex = e.getCause();
            if (ce.isInstance(ex))
                throw (E) ex;
            throw e;
        }
    }

    /**
     * <b>Terminal Operations Note</b>
     * <p>
     * Every terminal operation in {@link TryStream} can accept an exception class. Whenever a
     * throw happens anywhere in the stream chain using the trying functional interfaces, the
     * exception will be caught and rethrown as an {@link E}.
     */
    @SuppressWarnings("unchecked")
    public <E extends Throwable> T reduce(Class<E> ce, T identity, TryBiF<T, T, T, E> accumulator)
    throws E {
        try {
            return stream.reduce(identity, accumulator::apply);
        } catch (ResultException e) {
            var ex = e.getCause();
            if (ce.isInstance(ex))
                throw (E) ex;
            throw e;
        }
    }

    /**
     * <b>Terminal Operations Note</b>
     * <p>
     * Every terminal operation in {@link TryStream} can accept an exception class. Whenever a
     * throw happens anywhere in the stream chain using the trying functional interfaces, the
     * exception will be caught and rethrown as an {@link E}.
     */
    @SuppressWarnings("unchecked")
    public <E extends Throwable> Optional<T> reduce(Class<E> ce, TryBiF<T, T, T, E> accumulator)
    throws E {
        try {
            return stream.reduce(accumulator::apply);
        } catch (ResultException e) {
            var ex = e.getCause();
            if (ce.isInstance(ex))
                throw (E) ex;
            throw e;
        }
    }

    /**
     * <b>Terminal Operations Note</b>
     * <p>
     * Every terminal operation in {@link TryStream} can accept an exception class. Whenever a
     * throw happens anywhere in the stream chain using the trying functional interfaces, the
     * exception will be caught and rethrown as an {@link E}.
     */
    @SuppressWarnings("unchecked")
    public <U, E extends Throwable> U reduce(
        Class<E> ce,
        U identity,
        TryBiF<U, ? super T, U, E> accumulator,
        TryBiF<U, U, U, E> combiner
    ) throws E {
        try {
            return stream.reduce(identity, accumulator, combiner::apply);
        } catch (ResultException e) {
            var ex = e.getCause();
            if (ce.isInstance(ex))
                throw (E) ex;
            throw e;
        }
    }

    /**
     * <b>Terminal Operations Note</b>
     * <p>
     * Every terminal operation in {@link TryStream} can accept an exception class. Whenever a
     * throw happens anywhere in the stream chain using the trying functional interfaces, the
     * exception will be caught and rethrown as an {@link E}.
     */
    @SuppressWarnings("unchecked")
    public <E extends Throwable> Optional<T> min(
        Class<E> ce,
        TryBiF<? super T, ? super T, Integer, E> comparator
    ) throws E {
        try {
            return stream.min(comparator::apply);
        } catch (ResultException e) {
            var ex = e.getCause();
            if (ce.isInstance(ex))
                throw (E) ex;
            throw e;
        }
    }

    /**
     * <b>Terminal Operations Note</b>
     * <p>
     * Every terminal operation in {@link TryStream} can accept an exception class. Whenever a
     * throw happens anywhere in the stream chain using the trying functional interfaces, the
     * exception will be caught and rethrown as an {@link E}.
     */
    @SuppressWarnings("unchecked")
    public <E extends Throwable> Optional<T> max(
        Class<E> ce,
        TryBiF<? super T, ? super T, Integer, E> comparator
    ) throws E {
        try {
            return stream.min(comparator::apply);
        } catch (ResultException e) {
            var ex = e.getCause();
            if (ce.isInstance(ex))
                throw (E) ex;
            throw e;
        }
    }

    /**
     * <b>Terminal Operations Note</b>
     * <p>
     * Every terminal operation in {@link TryStream} can accept an exception class. Whenever a
     * throw happens anywhere in the stream chain using the trying functional interfaces, the
     * exception will be caught and rethrown as an {@link E}.
     */
    @SuppressWarnings({"unchecked", "UnreachableCode"})
    public <E extends Throwable> long count(Class<E> ce) throws E {
        try {
            return stream.count();
        } catch (ResultException e) {
            var ex = e.getCause();
            if (ce.isInstance(ex))
                throw (E) ex;
            throw e;
        }
    }

    /**
     * <b>Terminal Operations Note</b>
     * <p>
     * Every terminal operation in {@link TryStream} can accept an exception class. Whenever a
     * throw happens anywhere in the stream chain using the trying functional interfaces, the
     * exception will be caught and rethrown as an {@link E}.
     */
    @SuppressWarnings("unchecked")
    public <E extends Throwable> boolean anyMatch(Class<E> ce, TryP<? super T, E> predicate)
    throws E {
        try {
            return stream.anyMatch(predicate);
        } catch (ResultException e) {
            var ex = e.getCause();
            if (ce.isInstance(ex))
                throw (E) ex;
            throw e;
        }
    }

    /**
     * <b>Terminal Operations Note</b>
     * <p>
     * Every terminal operation in {@link TryStream} can accept an exception class. Whenever a
     * throw happens anywhere in the stream chain using the trying functional interfaces, the
     * exception will be caught and rethrown as an {@link E}.
     */
    @SuppressWarnings("unchecked")
    public <E extends Throwable> boolean allMatch(Class<E> ce, TryP<? super T, E> predicate)
    throws E {
        try {
            return stream.allMatch(predicate);
        } catch (ResultException e) {
            var ex = e.getCause();
            if (ce.isInstance(ex))
                throw (E) ex;
            throw e;
        }
    }

    /**
     * <b>Terminal Operations Note</b>
     * <p>
     * Every terminal operation in {@link TryStream} can accept an exception class. Whenever a
     * throw happens anywhere in the stream chain using the trying functional interfaces, the
     * exception will be caught and rethrown as an {@link E}.
     */
    @SuppressWarnings("unchecked")
    public <E extends Throwable> boolean noneMatch(Class<E> ce, TryP<? super T, E> predicate)
    throws E {
        try {
            return stream.noneMatch(predicate);
        } catch (ResultException e) {
            var ex = e.getCause();
            if (ce.isInstance(ex))
                throw (E) ex;
            throw e;
        }
    }

    /**
     * <b>Terminal Operations Note</b>
     * <p>
     * Every terminal operation in {@link TryStream} can accept an exception class. Whenever a
     * throw happens anywhere in the stream chain using the trying functional interfaces, the
     * exception will be caught and rethrown as an {@link E}.
     */
    @SuppressWarnings({"unchecked", "UnreachableCode"})
    public <E extends Throwable> Optional<T> findFirst(Class<E> ce) throws E {
        try {
            return stream.findFirst();
        } catch (ResultException e) {
            var ex = e.getCause();
            if (ce.isInstance(ex))
                throw (E) ex;
            throw e;
        }
    }

    /**
     * <b>Terminal Operations Note</b>
     * <p>
     * Every terminal operation in {@link TryStream} can accept an exception class. Whenever a
     * throw happens anywhere in the stream chain using the trying functional interfaces, the
     * exception will be caught and rethrown as an {@link E}.
     */
    @SuppressWarnings({"unchecked", "UnreachableCode"})
    public <E extends Throwable> Optional<T> findAny(Class<E> ce) throws E {
        try {
            return stream.findAny();
        } catch (ResultException e) {
            var ex = e.getCause();
            if (ce.isInstance(ex))
                throw (E) ex;
            throw e;
        }
    }

    /**
     * <b>Terminal Operations Note</b>
     * <p>
     * Every terminal operation in {@link TryStream} can accept an exception class. Whenever a
     * throw happens anywhere in the stream chain using the trying functional interfaces, the
     * exception will be caught and rethrown as an {@link E}.
     */
    @SuppressWarnings("unchecked")
    public <E extends Throwable> Iterator<T> iterator(Class<E> ce) throws E {
        try {
            return stream.iterator();
        } catch (ResultException e) {
            var ex = e.getCause();
            if (ce.isInstance(ex))
                throw (E) ex;
            throw e;
        }
    }

    /**
     * <b>Terminal Operations Note</b>
     * <p>
     * Every terminal operation in {@link TryStream} can accept an exception class. Whenever a
     * throw happens anywhere in the stream chain using the trying functional interfaces, the
     * exception will be caught and rethrown as an {@link E}.
     */
    @SuppressWarnings("unchecked")
    public <E extends Throwable> Spliterator<T> spliterator(Class<E> ce) throws E {
        try {
            return stream.spliterator();
        } catch (ResultException e) {
            var ex = e.getCause();
            if (ce.isInstance(ex))
                throw (E) ex;
            throw e;
        }
    }

    //
    // Try-Collectors
    //

    public static <T, R, E extends Throwable> Collector<T, R, R> collector(
        TryS<R, E> supplier,
        TryBiC<R, T, E> accumulator,
        TryBiF<R, R, R, E> combiner,
        Characteristics... characteristics
    ) {
        return Collector.of(
            supplier,
            accumulator,
            combiner::apply,
            characteristics
        );
    }

    public static <T, A, R, E extends Throwable> Collector<T, A, R> collector(
        TryS<A, E> supplier,
        TryBiC<A, T, E> accumulator,
        TryBiF<A, A, A, E> combiner,
        TryF<A, R, E> finisher,
        Characteristics... characteristics
    ) {
        return Collector.of(
            supplier,
            accumulator,
            combiner::apply,
            finisher,
            characteristics
        );
    }

    /**
     * Collect with a possibility of throwing an exception. Consider using
     * {@link #collector(TryS, TryBiC, TryBiF, TryF, Characteristics...)} and other
     * methods to create a collector that will work with this method.
     *
     * <p>
     * <b>Terminal Operations Note</b>
     * <p>
     * Every terminal operation in {@link TryStream} can accept an exception class. Whenever a
     * throw happens anywhere in the stream chain using the trying functional interfaces, the
     * exception will be caught and rethrown as an {@link E}.
     *
     * @param ce        Exception type to catch.
     * @param collector The collector that will work with this method.
     * @param <R>       Return type.
     * @param <A>       Accumulator type.
     * @param <E>       Error type.
     * @return The result of the collector.
     * @throws E Error type.
     */
    @SuppressWarnings("unchecked")
    public <R, A, E extends Throwable> R collect(
        Class<E> ce,
        Collector<? super T, A, R> collector
    ) throws E {
        try {
            return stream.collect(collector);
        } catch (ResultException e) {
            var ex = e.getCause();
            if (ce.isInstance(ex))
                throw (E) ex;
            throw e;
        }
    }

    @SuppressWarnings("unchecked")
    public <R, E extends Throwable> R collect(
        Class<E> ce,
        Supplier<R> supplier,
        BiConsumer<R, ? super T> accumulator,
        BiConsumer<R, R> combiner
    ) throws E {
        try {
            return stream.collect(supplier, accumulator, combiner);
        } catch (ResultException e) {
            var ex = e.getCause();
            if (ce.isInstance(ex))
                throw (E) ex;
            throw e;
        }
    }

    //
    // Stream
    //

    public TryStream<T> filter(Predicate<? super T> predicate) {
        return new TryStream<>(stream.filter(predicate));
    }

    public <R> TryStream<R> map(Function<? super T, ? extends R> mapper) {
        return new TryStream<>(stream.map(mapper));
    }

    public IntStream mapToInt(ToIntFunction<? super T> mapper) {
        return stream.mapToInt(mapper);
    }

    public LongStream mapToLong(ToLongFunction<? super T> mapper) {
        return stream.mapToLong(mapper);
    }

    public DoubleStream mapToDouble(ToDoubleFunction<? super T> mapper) {
        return stream.mapToDouble(mapper);
    }

    public <R> TryStream<R> flatMap(Function<? super T, ? extends Stream<? extends R>> mapper) {
        return new TryStream<>(stream.flatMap(mapper));
    }

    public IntStream flatMapToInt(Function<? super T, ? extends IntStream> mapper) {
        return stream.flatMapToInt(mapper);
    }

    public LongStream flatMapToLong(Function<? super T, ? extends LongStream> mapper) {
        return stream.flatMapToLong(mapper);
    }

    public DoubleStream flatMapToDouble(Function<? super T, ? extends DoubleStream> mapper) {
        return stream.flatMapToDouble(mapper);
    }

    public TryStream<T> distinct() {
        return new TryStream<>(stream.distinct());
    }

    public TryStream<T> sorted() {
        return new TryStream<>(stream.sorted());
    }

    public TryStream<T> sorted(Comparator<? super T> comparator) {
        return new TryStream<>(stream.sorted(comparator));
    }

    public TryStream<T> peek(Consumer<? super T> action) {
        return new TryStream<>(stream.peek(action));
    }

    public TryStream<T> limit(long maxSize) {
        return new TryStream<>(stream.limit(maxSize));
    }

    public TryStream<T> skip(long n) {
        return new TryStream<>(stream.skip(n));
    }

    public void forEach(Consumer<? super T> action) {
        stream.forEach(action);
    }

    public void forEachOrdered(Consumer<? super T> action) {
        stream.forEachOrdered(action);
    }

    public Object[] toArray() {
        return stream.toArray();
    }

    public <A> A[] toArray(IntFunction<A[]> generator) {
        return stream.toArray(generator);
    }

    public T reduce(T identity, BinaryOperator<T> accumulator) {
        return stream.reduce(identity, accumulator);
    }

    public Optional<T> reduce(BinaryOperator<T> accumulator) {
        return stream.reduce(accumulator);
    }

    public <U> U reduce(
        U identity,
        BiFunction<U, ? super T, U> accumulator,
        BinaryOperator<U> combiner
    ) {
        return stream.reduce(identity, accumulator, combiner);
    }

    public <R> R collect(
        Supplier<R> supplier,
        BiConsumer<R, ? super T> accumulator,
        BiConsumer<R, R> combiner
    ) {
        return stream.collect(supplier, accumulator, combiner);
    }

    public <R, A> R collect(Collector<? super T, A, R> collector) {
        return stream.collect(collector);
    }

    public Optional<T> min(Comparator<? super T> comparator) {
        return stream.min(comparator);
    }

    public Optional<T> max(Comparator<? super T> comparator) {
        return stream.min(comparator);
    }

    public long count() {
        return stream.count();
    }

    public boolean anyMatch(Predicate<? super T> predicate) {
        return stream.anyMatch(predicate);
    }

    public boolean allMatch(Predicate<? super T> predicate) {
        return stream.allMatch(predicate);
    }

    public boolean noneMatch(Predicate<? super T> predicate) {
        return stream.noneMatch(predicate);
    }

    public Optional<T> findFirst() {
        return stream.findFirst();
    }

    public Optional<T> findAny() {
        return stream.findAny();
    }

    public Iterator<T> iterator() {
        return stream.iterator();
    }

    public Spliterator<T> spliterator() {
        return stream.spliterator();
    }

    public boolean isParallel() {
        return stream.isParallel();
    }

    public TryStream<T> sequential() {
        return new TryStream<>(stream.sequential());
    }

    public TryStream<T> parallel() {
        return new TryStream<>(stream.parallel());
    }

    public TryStream<T> unordered() {
        return new TryStream<>(stream.unordered());
    }

    public TryStream<T> onClose(Runnable closeHandler) {
        return new TryStream<>(stream.onClose(closeHandler));
    }

    public void close() {
        stream.close();
    }
}
