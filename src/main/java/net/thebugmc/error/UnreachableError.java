package net.thebugmc.error;

/**
 * An error to cover the cases when reaching some line is not possible.
 *
 * <p>
 * Also known as "assert not reached" and "unexpected" (although latter usually means {@link IllegalArgumentException}).
 */
public final class UnreachableError extends Error {
    /**
     * Create a new {@link UnreachableError} with no message.
     */
    public UnreachableError() {
    }

    /**
     * Create a new {@link UnreachableError} with the reason why this error may not be reached. For
     * example, it could notify that an unexpected value inside a private method is used, or when
     * previous checks should already cover all cases and yet the throw of this error happened.
     *
     * @param message The reason why this error may not be reached.
     */
    public UnreachableError(String message) {
        super(message);
    }

    /**
     * Neat shorthand for creating the {@link UnreachableError}.
     *
     * @return {@link UnreachableError}
     * @see #UnreachableError()
     */
    public static UnreachableError unreachable() {
        return new UnreachableError();
    }

    /**
     * Neat shorthand for creating the {@link UnreachableError} with the reason why this error may
     * not be reached. For example, it could notify that an unexpected value inside a private method
     * is used, or when previous checks should already cover all cases and yet the throw of this
     * error happened.
     *
     * @param message The reason why this error may not be reached.
     * @return {@link UnreachableError}
     * @see #UnreachableError(String)
     */
    public static UnreachableError unreachable(String message) {
        return new UnreachableError(message);
    }
}
