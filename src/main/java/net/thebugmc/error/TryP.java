package net.thebugmc.error;

import java.util.function.Predicate;

@FunctionalInterface
public interface TryP<T, E extends Throwable> extends Predicate<T> {
    boolean tryTest(T t) throws E;

    @SuppressWarnings("OverlyBroadCatchBlock")
    default boolean test(T t) {
        try {
            return tryTest(t);
        } catch (Throwable e) {
            throw new ResultException(e);
        }
    }

    static <T, E extends Throwable> TryP<T, E> of(Predicate<T> f) {
        return f::test;
    }
}
