package net.thebugmc.error;

import java.util.function.BiPredicate;

@FunctionalInterface
public interface TryBiP<T, U, E extends Throwable> extends BiPredicate<T, U> {
    boolean tryTest(T t, U u) throws E;

    @SuppressWarnings("OverlyBroadCatchBlock")
    default boolean test(T t, U u) {
        try {
            return tryTest(t, u);
        } catch (Throwable e) {
            throw new ResultException(e);
        }
    }

    static <T, U, E extends Throwable> TryBiP<T, U, E> of(BiPredicate<T, U> f) {
        return f::test;
    }
}
